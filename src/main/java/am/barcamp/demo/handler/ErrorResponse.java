package am.barcamp.demo.handler;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Getter;
import org.springframework.http.HttpStatus;

import java.util.Set;

@Getter
@JsonInclude(JsonInclude.Include.NON_NULL)
class ErrorResponse {
    private HttpStatus code;
    private String message;
    private Set<FieldError> fieldErrors;

    ErrorResponse(String message) {
        this(HttpStatus.BAD_REQUEST, message);
    }

    ErrorResponse(HttpStatus code, String message) {
        this.code = code;
        this.message = message;
    }

    ErrorResponse(Set<FieldError> fieldErrors) {
        this.code = HttpStatus.BAD_REQUEST;
        this.message = "Payload Validation Failed";
        this.fieldErrors = fieldErrors;
    }

    public String getCode() {
        return String.format("%s %s", code.value(), code.getReasonPhrase());
    }
}
