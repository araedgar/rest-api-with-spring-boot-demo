package am.barcamp.demo.handler;

import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
@Getter
class FieldError {
    private String field;
    private String message;
}