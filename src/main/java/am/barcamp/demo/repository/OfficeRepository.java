package am.barcamp.demo.repository;

import am.barcamp.demo.model.Office;
import am.barcamp.demo.model.QOffice;
import com.querydsl.core.types.dsl.StringExpression;
import com.querydsl.core.types.dsl.StringPath;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.data.querydsl.binding.QuerydslBinderCustomizer;
import org.springframework.data.querydsl.binding.QuerydslBindings;
import org.springframework.data.querydsl.binding.SingleValueBinding;

public interface OfficeRepository extends JpaRepository<Office, Long>, QuerydslPredicateExecutor<Office>,
        QuerydslBinderCustomizer<QOffice> {

    @Override
    default void customize(QuerydslBindings bindings, QOffice root) {
        bindings.bind(String.class).first((SingleValueBinding<StringPath, String>) StringExpression::containsIgnoreCase);
    }
}
