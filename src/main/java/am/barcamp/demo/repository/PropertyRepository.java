package am.barcamp.demo.repository;

import am.barcamp.demo.model.Property;
import com.querydsl.core.types.Predicate;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;

import javax.annotation.Nonnull;
import java.util.List;

public interface PropertyRepository extends JpaRepository<Property, Long>, QuerydslPredicateExecutor<Property> {
    @Override
    @Nonnull
    List<Property> findAll(@Nonnull Predicate predicate);
}
