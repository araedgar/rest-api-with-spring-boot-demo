package am.barcamp.demo.converter;

import am.barcamp.demo.util.AutowireHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.Map;
import java.util.Set;

public class MapToJsonConverter implements AttributeConverter<Map<String, String>, String> {
    @Override
    public String convertToDatabaseColumn(Map<String, String> attribute) {
        try {
            return AutowireHelper.getBean(ObjectMapper.class).writeValueAsString(attribute);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Failed to convert Map<String, String> to json string");
        }
    }

    @Override
    public Map<String, String> convertToEntityAttribute(String dbData) {
        try {
            return AutowireHelper.getBean(ObjectMapper.class).readValue(dbData, new TypeReference<Map<String, String>>() {
            });
        } catch (IOException e) {
            throw new IllegalStateException("Failed to convert json string to Map<String, String>");
        }
    }
}
