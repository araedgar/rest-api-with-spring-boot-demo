package am.barcamp.demo.converter;

import am.barcamp.demo.util.AutowireHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;
import java.io.IOException;
import java.util.Set;

@Converter(autoApply = true)
public class LongSetToJsonConverter implements AttributeConverter<Set<Long>, String> {
    @Override
    public String convertToDatabaseColumn(Set<Long> attribute) {
        try {
            return AutowireHelper.getBean(ObjectMapper.class).writeValueAsString(attribute);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Failed to convert Set<Number> to json string");
        }
    }

    @Override
    public Set<Long> convertToEntityAttribute(String dbData) {
        try {
            return AutowireHelper.getBean(ObjectMapper.class).readValue(dbData, new TypeReference<Set<Long>>() {
            });
        } catch (IOException e) {
            throw new IllegalStateException("Failed to convert json string to Set<Number>");
        }
    }
}
