package am.barcamp.demo.converter;

import am.barcamp.demo.util.AutowireHelper;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.persistence.AttributeConverter;
import java.io.IOException;
import java.util.Set;

public class StringSetToJsonConverter implements AttributeConverter<Set<String>, String> {
    @Override
    public String convertToDatabaseColumn(Set<String> attribute) {
        try {
            return AutowireHelper.getBean(ObjectMapper.class).writeValueAsString(attribute);
        } catch (JsonProcessingException e) {
            throw new IllegalStateException("Failed to convert Set<Number> to json string");
        }
    }

    @Override
    public Set<String> convertToEntityAttribute(String dbData) {
        try {
            return AutowireHelper.getBean(ObjectMapper.class).readValue(dbData, new TypeReference<Set<String>>() {
            });
        } catch (IOException e) {
            throw new IllegalStateException("Failed to convert json string to Set<Number>");
        }
    }
}
