package am.barcamp.demo.service.impl;

import am.barcamp.demo.service.ConverterService;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;

import static java.util.Optional.ofNullable;

@Component("converterService")
public class ConverterServiceImpl implements ConverterService {
    @Override
    public ZonedDateTime convertDateTime(LocalDateTime dateTime) {
        return ofNullable(dateTime).map(dt -> ZonedDateTime.of(dt, ZoneId.systemDefault())
                .withZoneSameInstant(ZoneOffset.UTC)).orElse(null);
    }
}
