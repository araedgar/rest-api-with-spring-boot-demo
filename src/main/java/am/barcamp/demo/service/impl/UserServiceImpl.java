package am.barcamp.demo.service.impl;

import am.barcamp.demo.binding.UserPayload;
import am.barcamp.demo.binding.UserUpdatePayload;
import am.barcamp.demo.model.TwoFactorType;
import am.barcamp.demo.model.User;
import am.barcamp.demo.model.UserRole;
import am.barcamp.demo.repository.UserRepository;
import am.barcamp.demo.service.OfficeService;
import am.barcamp.demo.service.UserService;
import lombok.RequiredArgsConstructor;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service("userService")
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {
    private final UserRepository userRepository;
    private final OfficeService officeService;
    private final PasswordEncoder passwordEncoder;

    @Override
    @Transactional
    public User create(UserPayload payload) {
        final User user = new User();
        BeanUtils.copyProperties(payload, user);
        user.setLocked(false);
        user.setEnabled(true);
        user.setRole(UserRole.ROLE_OFFICE_MANAGER);
        user.setPassword(passwordEncoder.encode(payload.getPassword()));
        user.setOffice(officeService.getById(payload.getOfficeId()));
        return userRepository.save(user);
    }

    @Override
    @Transactional
    public User update(User user, UserUpdatePayload userPayload) {
        if (TwoFactorType.APP.equals(userPayload.getTwoFactorType()) && user.getSecret() == null) {
            user.setSecret(RandomStringUtils.random(10, true, true).toUpperCase());
        }
        BeanUtils.copyProperties(userPayload, user);
        return userRepository.save(user);
    }

    @Override
    public User getById(Long id) {
        return userRepository.getOne(id);
    }
}
