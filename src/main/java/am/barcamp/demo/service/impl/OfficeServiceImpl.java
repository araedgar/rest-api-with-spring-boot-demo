package am.barcamp.demo.service.impl;

import am.barcamp.demo.binding.OfficePayload;
import am.barcamp.demo.model.Office;
import am.barcamp.demo.model.User;
import am.barcamp.demo.repository.OfficeRepository;
import am.barcamp.demo.service.OfficeService;
import com.querydsl.core.types.Predicate;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.EntityNotFoundException;

@Service("officeService")
@RequiredArgsConstructor
public class OfficeServiceImpl implements OfficeService {
    private final OfficeRepository officeRepository;

    @Override
    public Page<Office> getAll(User user, Predicate predicate, Pageable pageable) {
        return officeRepository.findAll(predicate, pageable);
    }

    @Override
    public Office getById(Long id) {
        return officeRepository.getOne(id);
    }

    @Override
    public Office getOneForOffice(User user) {
        return officeRepository.findById(user.getOffice().getId()).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    @Transactional
    public Office create(OfficePayload officePayload) {
        final Office office = new Office();
        BeanUtils.copyProperties(officePayload, office);
        return officeRepository.save(office);
    }

    @Override
    @Transactional
    public Office update(User user, OfficePayload officePayload) {
        final Office office = getOneForOffice(user);
        BeanUtils.copyProperties(officePayload, office);
        return officeRepository.save(office);
    }

    @Override
    public void delete(User user, Long id) {
        officeRepository.delete(getById(id));
    }
}
