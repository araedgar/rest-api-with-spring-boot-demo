package am.barcamp.demo.service.impl;

import am.barcamp.demo.binding.PropertyPayload;
import am.barcamp.demo.model.Property;
import am.barcamp.demo.model.QProperty;
import am.barcamp.demo.model.User;
import am.barcamp.demo.repository.PropertyRepository;
import am.barcamp.demo.response.DeleteResponse;
import am.barcamp.demo.service.PropertyService;
import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.BeanUtils;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.persistence.EntityNotFoundException;
import java.util.Currency;
import java.util.List;
import java.util.Set;

import static java.util.stream.Collectors.toSet;

@Service("propertyService")
@RequiredArgsConstructor
public class PropertyServiceImpl implements PropertyService {
    private final PropertyRepository propertyRepository;

    @Override
    public Property create(User user, PropertyPayload payload) {
        final Property property = new Property();
        BeanUtils.copyProperties(payload, property);
        property.setOffice(user.getOffice());
        property.setCurrency(Currency.getInstance(payload.getCurrency()));
        return propertyRepository.save(property);
    }

    @Override
    public Property update(User user, Long id, PropertyPayload payload) {
        final Property property = getById(user, id);
        BeanUtils.copyProperties(payload, property);
        property.setCurrency(Currency.getInstance(payload.getCurrency()));
        return propertyRepository.save(property);
    }

    @Override
    public Page<Property> getAll(User user, Predicate predicate, Pageable pageable) {
        final BooleanExpression filter = QProperty.property.office.id.eq(user.getOffice().getId()).and(predicate);
        return propertyRepository.findAll(filter, pageable);
    }

    @Override
    public Property getById(User user, Long id) {
        final BooleanExpression filter = QProperty.property.office.id.eq(user.getOffice().getId())
                .and(QProperty.property.id.eq(id));
        return propertyRepository.findOne(filter).orElseThrow(EntityNotFoundException::new);
    }

    @Override
    public void delete(User user, Long id) {
        propertyRepository.delete(getById(user, id));
    }

    @Override
    public DeleteResponse deleteMultiple(User user, Set<Long> ids) {
        final BooleanExpression filter = QProperty.property.office.id.eq(user.getOffice().getId())
                .and(QProperty.property.id.in(ids));
        final List<Property> foundProperties = propertyRepository.findAll(filter);
        final Set<Long> existingIds = foundProperties.stream().map(Property::getId).collect(toSet());
        final Set<Long> nonExistingIds = ids.stream().filter(id -> !existingIds.contains(id)).collect(toSet());
        propertyRepository.deleteAll(foundProperties);
        return DeleteResponse.builder().deleted(existingIds).notFound(nonExistingIds).build();
    }
}
