package am.barcamp.demo.service.impl;

import am.barcamp.demo.model.TwoFactorType;
import am.barcamp.demo.model.User;
import am.barcamp.demo.service.TwoFactorAuthenticationService;
import am.barcamp.demo.util.TOTP;
import org.apache.commons.codec.EncoderException;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Service;

@Service
public class TwoFactorAuthenticationServiceImpl implements TwoFactorAuthenticationService {
    @Override
    public boolean isValidCode(User user, String code) {
        return TwoFactorType.APP.equals(user.getTwoFactorType()) ? isValidAppCode(code, user.getSecret())
                : isValidSmsCode(code, user.getPhone());
    }

    @Override
    public void generateVerificationCode(User user) {
        if (TwoFactorType.APP.equals(user.getTwoFactorType())) return;
        makeGenerateRequest();
    }

    private void makeGenerateRequest() {
        //TODO make http request to SMS 2FA API to generate code
    }

    private boolean isValidSmsCode(String code, String phone) {
        //TODO make http request to SMS 2FA API to verify code
        return true;
    }

    private static boolean isValidAppCode(String code, String secret) {
        return code.equals(generateTotpBySecret(secret));
    }

    private static String generateTotpBySecret(String secret) {
        try {
            final char[] secretEncoded = (char[]) new Hex().encode(secret);
            final String timeEncoded = Long.toHexString(System.currentTimeMillis() / 30000L);
            return TOTP.generateTOTP(String.copyValueOf(secretEncoded), timeEncoded, "6");
        } catch (EncoderException e) {
            return null;
        }
    }
}
