package am.barcamp.demo.service;

import am.barcamp.demo.binding.OfficePayload;
import am.barcamp.demo.model.Office;
import am.barcamp.demo.model.User;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface OfficeService {

    /**
     * Retrieves offices for search predicate with pagination
     *
     * @param user      Office user
     * @param predicate Search predicate
     * @param pageable  Pagination
     * @return Offices
     */
    Page<Office> getAll(User user, Predicate predicate, Pageable pageable);

    /**
     * @param id Office id
     * @return Office
     */
    Office getById(Long id);

    /**
     * Retrieves office of user
     *
     * @param user Office user
     * @return Office
     */
    Office getOneForOffice(User user);

    /**
     * Creates office for provided request payload
     *
     * @param officePayload Request payload
     * @return Office
     * @see OfficePayload
     */
    Office create(OfficePayload officePayload);

    /**
     * Updates office
     *
     * @param user          Office user
     * @param officePayload Request payload
     * @return Updated office
      */
    Office update(User user, OfficePayload officePayload);

    /**
     * Removes office
     *
     * @param user Office user
     * @param id   Office id
     */
    void delete(User user, Long id);
}
