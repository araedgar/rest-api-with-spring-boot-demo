package am.barcamp.demo.service;


import am.barcamp.demo.model.User;

public interface TwoFactorAuthenticationService {
    void generateVerificationCode(User user);

    boolean isValidCode(User user, String code);
}
