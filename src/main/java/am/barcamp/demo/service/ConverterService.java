package am.barcamp.demo.service;

import java.time.LocalDateTime;
import java.time.ZonedDateTime;

public interface ConverterService {

    /**
     * Converts local date time to zoned date time
     *
     * @param dateTime
     * @return converted zoned date time
     */
    ZonedDateTime convertDateTime(LocalDateTime dateTime);
}
