package am.barcamp.demo.service;

import am.barcamp.demo.binding.PropertyPayload;
import am.barcamp.demo.model.Property;
import am.barcamp.demo.model.User;
import am.barcamp.demo.response.DeleteResponse;
import com.querydsl.core.types.Predicate;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Set;

public interface PropertyService {
    Property create(User user, PropertyPayload payload);

    Property update(User user, Long id, PropertyPayload payload);

    Page<Property> getAll(User user, Predicate predicate, Pageable pageable);

    Property getById(User user, Long id);

    void delete(User user, Long id);

    DeleteResponse deleteMultiple(User user, Set<Long> ids);
}
