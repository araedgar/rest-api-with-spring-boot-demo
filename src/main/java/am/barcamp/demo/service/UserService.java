package am.barcamp.demo.service;

import am.barcamp.demo.binding.UserPayload;
import am.barcamp.demo.binding.UserUpdatePayload;
import am.barcamp.demo.model.Office;
import am.barcamp.demo.model.User;

public interface UserService {

    /**
     * @param userPayload Request payload
     * @return Created office user
     */
    User create(UserPayload userPayload);

    /**
     * @param user        authenticated user
     * @param userPayload Request payload
     * @return updated user
     */
    User update(User user, UserUpdatePayload userPayload);

    User getById(Long id);
}
