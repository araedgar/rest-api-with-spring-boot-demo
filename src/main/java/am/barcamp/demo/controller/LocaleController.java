package am.barcamp.demo.controller;

import am.barcamp.demo.util.LocaleProvider;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@RestController
@RequestMapping("/locale")
@Api(value = "/locale", description = "Locale API for retrieving countries, currencies and timezones")
public class LocaleController {
    @ApiOperation(value = "Get all countries of the world", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/countries")
    public Set<String> getAvailableCountryNames() {
        return LocaleProvider.COUNTRY_NAMES;
    }

    @ApiOperation(value = "Get all currencies of the world", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/currencies")
    public Set<String> getAvailableCurrencyCodes() {
        return LocaleProvider.CURRENCY_CODES;
    }

    @ApiOperation(value = "Get all timezones of the world", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping("/timezones")
    public Set<String> getAvailableTimezones() {
        return LocaleProvider.TIMEZONES;
    }
}
