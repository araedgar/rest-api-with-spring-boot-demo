package am.barcamp.demo.controller;

import am.barcamp.demo.annotation.CurrentUser;
import am.barcamp.demo.binding.IdSetPayload;
import am.barcamp.demo.binding.PropertyPayload;
import am.barcamp.demo.model.Property;
import am.barcamp.demo.model.User;
import am.barcamp.demo.response.DeleteResponse;
import am.barcamp.demo.response.PropertyResponse;
import am.barcamp.demo.service.PropertyService;
import com.querydsl.core.types.Predicate;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.binding.QuerydslPredicate;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

import static am.barcamp.demo.util.ProjectionMapper.map;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequiredArgsConstructor
@RequestMapping("/properties")
@Api(value = "/user", description = "Property API")
public class PropertyController {
    private final PropertyService propertyService;

    @PostMapping
    @ApiOperation(value = "Create property", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public PropertyResponse create(@CurrentUser User user, @Valid @RequestBody PropertyPayload payload) {
        Property source = propertyService.create(user, payload);
        return map(PropertyResponse.class, source);
    }

    @PutMapping("/{id}")
    @ApiOperation(value = "Update property by id", consumes = APPLICATION_JSON_VALUE, produces = APPLICATION_JSON_VALUE)
    public PropertyResponse update(@CurrentUser User user, @PathVariable Long id, @Valid @RequestBody PropertyPayload payload) {
        return map(PropertyResponse.class, propertyService.update(user, id, payload));
    }

    @GetMapping
    @ApiOperation(value = "Get all properties of user office", produces = APPLICATION_JSON_VALUE)
    public Page<PropertyResponse> getAll(@CurrentUser User user,
                                         @QuerydslPredicate(root = Property.class) Predicate predicate,
                                         Pageable pageable) {
        return propertyService.getAll(user, predicate, pageable).map(property -> map(PropertyResponse.class, property));
    }

    @GetMapping("/{id}")
    @ApiOperation(value = "Get property by id", produces = APPLICATION_JSON_VALUE)
    public PropertyResponse getById(@CurrentUser User user, @PathVariable Long id) {
        return map(PropertyResponse.class, propertyService.getById(user, id));
    }

    @DeleteMapping("/{id}")
    @ApiOperation(value = "Delete property by id")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void remove(@CurrentUser User user, @PathVariable Long id) {
        propertyService.delete(user, id);
    }

    @PostMapping("/delete")
    @ApiOperation(value = "Delete multiple properties by ids", consumes = APPLICATION_JSON_VALUE,
            produces = APPLICATION_JSON_VALUE)
    public DeleteResponse removeMultiple(@CurrentUser User user, @Valid @RequestBody IdSetPayload payload) {
        return propertyService.deleteMultiple(user, payload.getIds());
    }
}
