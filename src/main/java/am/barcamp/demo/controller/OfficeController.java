package am.barcamp.demo.controller;

import am.barcamp.demo.annotation.CurrentUser;
import am.barcamp.demo.binding.OfficePayload;
import am.barcamp.demo.model.User;
import am.barcamp.demo.response.OfficeResponse;
import am.barcamp.demo.service.OfficeService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.annotation.security.RolesAllowed;
import javax.validation.Valid;

import static am.barcamp.demo.util.ProjectionMapper.map;


@RestController
@RequiredArgsConstructor
@RequestMapping("/office")
@Api(value = "/office", description = "Office API")
public class OfficeController {
    private final OfficeService officeService;

    @PostMapping
    @ApiOperation(value = "Create office and its user", produces= MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.CREATED)
    public OfficeResponse create(@Valid @RequestBody OfficePayload officePayload) {
        return map(OfficeResponse.class, officeService.create(officePayload));
    }

    @GetMapping
    @ApiOperation(value = "Get office of current user", produces = MediaType.APPLICATION_JSON_VALUE)
    @RolesAllowed("ROLE_OFFICE_MANAGER")
    public OfficeResponse getOne(@CurrentUser User user) {
        return map(OfficeResponse.class, officeService.getOneForOffice(user));
    }

    @PutMapping
    @ApiOperation(value = "Update office of current user", produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE, response = OfficeResponse.class)
    @RolesAllowed("ROLE_OFFICE_MANAGER")
    public OfficeResponse update(@CurrentUser User user, @Valid @RequestBody OfficePayload officePayload) {
        return map(OfficeResponse.class, officeService.update(user, officePayload));
    }
}
