package am.barcamp.demo.controller;

import am.barcamp.demo.annotation.CurrentUser;
import am.barcamp.demo.binding.UserPayload;
import am.barcamp.demo.binding.UserUpdatePayload;
import am.barcamp.demo.model.User;
import am.barcamp.demo.response.UserResponse;
import am.barcamp.demo.service.UserService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.springframework.context.MessageSource;
import org.springframework.data.projection.ProjectionFactory;
import org.springframework.http.MediaType;
import org.springframework.mobile.device.Device;
import org.springframework.mobile.device.DevicePlatform;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Locale;
import java.util.Map;

@RestController
@RequiredArgsConstructor
@RequestMapping("/user")
@Api(value = "/user", description = "User API")
public class UserController {
    private final UserService userService;
    private final ProjectionFactory projectionFactory;
    private final MessageSource messageSource;

    @PostMapping
    @ApiOperation(value = "Create user", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserResponse create(@Valid @RequestBody UserPayload payload) {
        return projectionFactory.createProjection(UserResponse.class, userService.create(payload));
    }

    @ApiOperation(value = "Get user", produces = MediaType.APPLICATION_JSON_VALUE)
    @GetMapping
    public UserResponse get(@CurrentUser User user) {
        return projectionFactory.createProjection(UserResponse.class, user);
    }

    @ApiOperation(value = "Update user", produces = MediaType.APPLICATION_JSON_VALUE)
    @PutMapping
    public UserResponse update(@CurrentUser User user, @Valid @RequestBody UserUpdatePayload payload) {
        return projectionFactory.createProjection(UserResponse.class, userService.update(user, payload));
    }

    @GetMapping("/demo")
    public Map<String, String> localizationDemo(@CurrentUser User user, Device device, Locale locale) {
        if (device.isNormal() && device.getDevicePlatform().equals(DevicePlatform.UNKNOWN)) {
            String prefix = messageSource.getMessage("male-prefix", null, locale);
            return Map.of("user", String.format("%s %s", prefix, user.getLastName()));
        }
        return null;
    }
}
