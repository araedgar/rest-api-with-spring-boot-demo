package am.barcamp.demo.model;

import am.barcamp.demo.annotation.ValidCountry;
import am.barcamp.demo.annotation.ValidTimezone;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Embeddable
@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Address {
    private String address1;
    private String address2;
    @NotBlank
    @ApiModelProperty(required = true)
    @ValidCountry
    @Column(nullable = false)
    private String country;
    @NotBlank
    @ApiModelProperty(required = true)
    @Column(nullable = false)
    private String city;
    @ValidTimezone
    private String timezone;
    @NotBlank
    @ApiModelProperty(required = true)
    @Column(nullable = false)
    private String phone;
    @NotNull
    @Email
    @ApiModelProperty(required = true)
    @Column(nullable = false)
    private String email;
}
