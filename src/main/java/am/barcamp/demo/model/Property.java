package am.barcamp.demo.model;

import am.barcamp.demo.converter.CurrencyToStringConverter;
import am.barcamp.demo.converter.MapToJsonConverter;
import lombok.*;
import org.springframework.data.annotation.CreatedBy;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedBy;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.Currency;
import java.util.Map;
import java.util.Optional;

@Entity
@Getter
@Setter
@Builder
@AllArgsConstructor
@ToString(exclude = {"office"})
@EqualsAndHashCode(of = "sku")
@EntityListeners(AuditingEntityListener.class)
public class Property {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String name;
    @Column(nullable = false)
    private String sku;
    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private PropertyType propertyType;
    private String color;
    private BigDecimal price;
    @Convert(converter = CurrencyToStringConverter.class)
    private Currency currency;
    private int quantity;
    @ManyToOne(fetch = FetchType.LAZY)
    private Office office;

    @CreatedDate
    private LocalDateTime createdDate;
    @LastModifiedDate
    private LocalDateTime lastModifiedDate;
    @CreatedBy
    private Long createdBy;
    @LastModifiedBy
    private Long lastModifiedBy;

    public Property() {
    }

    public BigDecimal getCost() {
        return Optional.ofNullable(price).map(p -> p.multiply(BigDecimal.valueOf(quantity))).orElse(BigDecimal.ZERO);
    }
}
