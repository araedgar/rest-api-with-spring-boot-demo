package am.barcamp.demo.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonEnumDefaultValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum PropertyType {
    FURNITURE("furniture"),
    ELECTRONICS("electronics"),
    COMPUTER("computer"),
    @JsonEnumDefaultValue
    OTHER("other");

    private String value;

    @JsonCreator
    PropertyType(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }
}
