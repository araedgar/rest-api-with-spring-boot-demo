package am.barcamp.demo.model;

import lombok.*;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.Set;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode(of = "name")
@EntityListeners(AuditingEntityListener.class)
public class Office {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, unique = true)
    private String name;

    @Embedded
    @Builder.Default
    private Address address = new Address();

    @CreatedDate
    private LocalDateTime createdDate;

    @OneToMany(mappedBy = "office", orphanRemoval = true)
    private Set<Property> properties;

    @OneToMany(mappedBy = "office", orphanRemoval = true)
    private Set<User> users;
}
