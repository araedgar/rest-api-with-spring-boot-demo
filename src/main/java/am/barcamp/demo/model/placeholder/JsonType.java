package am.barcamp.demo.model.placeholder;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonValue;

public enum JsonType {
    TEXT(JsonType.TEXT_VALUE),
    NUMBER(JsonType.NUMBER_VALUE),
    DATE(JsonType.DATE_VALUE);

    private String value;

    @JsonCreator
    JsonType(String value) {
        this.value = value;
    }

    @JsonValue
    public String getValue() {
        return value;
    }

    public static final String TEXT_VALUE = "text";
    public static final String NUMBER_VALUE = "number";
    public static final String DATE_VALUE = "date";
}
