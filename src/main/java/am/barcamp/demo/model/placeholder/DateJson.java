package am.barcamp.demo.model.placeholder;

import lombok.Builder;

import java.time.LocalDate;

public class DateJson extends PolymorphicJson<LocalDate> {
    private LocalDate defaultValue;

    public DateJson() {
    }

    @Builder
    public DateJson(String name, String displayName, JsonType type, LocalDate defaultValue) {
        super(name, displayName, type);
        this.defaultValue = defaultValue;
    }

    @Override
    public LocalDate getDefaultValue() {
        return defaultValue;
    }
}
