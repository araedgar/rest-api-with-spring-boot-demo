package am.barcamp.demo.model.placeholder;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import io.swagger.annotations.ApiModelProperty;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

import static am.barcamp.demo.model.placeholder.JsonType.*;

@JsonTypeInfo(use = JsonTypeInfo.Id.NAME, include = JsonTypeInfo.As.PROPERTY, property = "type", visible = true)
@JsonSubTypes({
        @JsonSubTypes.Type(value = DateJson.class, name = DATE_VALUE),
        @JsonSubTypes.Type(value = NumberJson.class, name = NUMBER_VALUE),
        @JsonSubTypes.Type(value = TextJson.class, name = TEXT_VALUE)
})
@EqualsAndHashCode(of = "name")
public abstract class PolymorphicJson<T> {
    @NotBlank
    @ApiModelProperty(required = true)
    private String name;
    private String displayName;
    @NotNull
    @ApiModelProperty(required = true)
    private JsonType type;

    public PolymorphicJson() {
    }

    public PolymorphicJson(@NotBlank String name, String displayName, @NotNull JsonType type) {
        this.name = name;
        this.displayName = displayName;
        this.type = type;
    }

    public String getName() {
        return this.name;
    }

    public String getDisplayName() {
        return this.displayName;
    }

    public JsonType getType() {
        return this.type;
    }

    public abstract T getDefaultValue();
}
