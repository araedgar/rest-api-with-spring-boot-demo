package am.barcamp.demo.model.placeholder;

import lombok.Builder;

import java.math.BigDecimal;

public class NumberJson extends PolymorphicJson<BigDecimal> {
    private BigDecimal defaultValue;

    public NumberJson() {
    }

    @Builder
    public NumberJson(String name, String displayName, JsonType type, BigDecimal defaultValue) {
        super(name, displayName, type);
        this.defaultValue = defaultValue;
    }

    @Override
    public BigDecimal getDefaultValue() {
        return defaultValue;
    }
}