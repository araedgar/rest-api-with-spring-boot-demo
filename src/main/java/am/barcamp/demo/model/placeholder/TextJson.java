package am.barcamp.demo.model.placeholder;

import lombok.Builder;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;


public class TextJson extends PolymorphicJson<String> {
    private String defaultValue;

    public TextJson() {
    }

    @Builder
    public TextJson(@NotBlank String name, String displayName, @NotNull JsonType type, String defaultValue) {
        super(name, displayName, type);
        this.defaultValue = defaultValue;
    }

    @Override
    public String getDefaultValue() {
        return defaultValue;
    }
}
