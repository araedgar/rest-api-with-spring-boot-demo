package am.barcamp.demo.model;

public enum TwoFactorType {
    APP,
    SMS;
}