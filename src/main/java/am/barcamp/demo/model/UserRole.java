package am.barcamp.demo.model;

public enum UserRole {
    ROLE_ADMIN,
    ROLE_OFFICE_MANAGER;
}