package am.barcamp.demo.response;

import am.barcamp.demo.model.Address;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.time.ZonedDateTime;

public interface OfficeResponse {
    Long getId();

    String getName();

    Address getAddress();

    @Value("#{@converterService.convertDateTime(target.getCreatedDate())}")
    ZonedDateTime getCreatedDate();
}
