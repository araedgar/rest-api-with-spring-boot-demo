package am.barcamp.demo.response;

import am.barcamp.demo.model.UserRole;
import org.springframework.beans.factory.annotation.Value;

import java.time.LocalDateTime;

public interface UserResponse {
    Long getId();

    String getFirstName();

    String getLastName();

    String getUsername();

    UserRole getRole();

    String getPhone();

    String getProfilePic();

    String getEncodedSecret();

    LocalDateTime getCreatedDate();

    @Value("#{target.getOffice()?.getId()}")
    Long getOfficeId();
}
