package am.barcamp.demo.response;

import am.barcamp.demo.model.PropertyType;
import am.barcamp.demo.model.User;
import org.springframework.beans.factory.annotation.Value;

import java.math.BigDecimal;
import java.time.ZonedDateTime;
import java.util.Currency;
import java.util.Map;

public interface PropertyResponse {
    Long getId();

    String getName();

    String getSku();

    String getColor();

    PropertyType getPropertyType();

    BigDecimal getPrice();

    Currency getCurrency();

    int getQuantity();

    BigDecimal getCost();

    @Value("#{@converterService.convertDateTime(target.getCreatedDate())}")
    ZonedDateTime getCreatedDate();

    @Value("#{@converterService.convertDateTime(target.getLastModifiedDate())}")
    ZonedDateTime getLastModifiedDate();

    Long getCreatedBy();

    Long getLastModifiedBy();
}
