package am.barcamp.demo.annotation;



import am.barcamp.demo.validation.TimezoneValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = {TimezoneValidator.class})
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidTimezone {
    String message() default "{am.barcamp.demo.annotation.ValidTimezone.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
