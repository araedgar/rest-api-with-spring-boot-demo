package am.barcamp.demo.annotation;


import am.barcamp.demo.validation.RGBValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = {RGBValidator.class})
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidRGB {
    String message() default "{am.barcamp.demo.annotation.ValidRGB.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
