package am.barcamp.demo.annotation;


import am.barcamp.demo.validation.PasswordConfirmValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = {PasswordConfirmValidator.class})
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface PasswordConfirm {
    String message() default "{am.barcamp.demo.annotation.PasswordConfirm.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
