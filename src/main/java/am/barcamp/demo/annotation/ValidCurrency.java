package am.barcamp.demo.annotation;


import am.barcamp.demo.validation.CurrencyValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = {CurrencyValidator.class})
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidCurrency {
    String message() default "{am.barcamp.demo.annotation.ValidCurrency.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
