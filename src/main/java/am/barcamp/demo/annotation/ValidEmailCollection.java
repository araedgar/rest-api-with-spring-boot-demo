package am.barcamp.demo.annotation;


import am.barcamp.demo.validation.EmailCollectionValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = {EmailCollectionValidator.class})
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidEmailCollection {

    String message() default "{am.barcamp.demo.annotation.ValidEmailCollection.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}