package am.barcamp.demo.annotation;


import am.barcamp.demo.validation.Base64ImageValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Constraint(validatedBy = {Base64ImageValidator.class})
@Target(ElementType.FIELD)
@Documented
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidBase64Image {
    String message() default "{am.barcamp.demo.annotation.ValidBase64Image.message}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
