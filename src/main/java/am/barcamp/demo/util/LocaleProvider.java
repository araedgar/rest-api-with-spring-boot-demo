package am.barcamp.demo.util;

import org.springframework.util.StringUtils;

import java.text.DateFormat;
import java.util.*;

import static java.util.stream.Collectors.toCollection;

public interface LocaleProvider {
    Set<String> COUNTRY_NAMES = Arrays.stream(DateFormat.getAvailableLocales())
            .filter(locale -> !StringUtils.isEmpty(locale.getDisplayCountry()))
            .map(Locale::getDisplayCountry)
            .collect(toCollection(TreeSet::new));
    Set<String> CURRENCY_CODES = Currency.getAvailableCurrencies().stream()
            .map(Currency::getCurrencyCode)
            .collect(toCollection(TreeSet::new));
    Set<String> TIMEZONES = Arrays.stream(TimeZone.getAvailableIDs())
            .collect(toCollection(TreeSet::new));
}
