package am.barcamp.demo.util;

import org.springframework.util.InvalidMimeTypeException;
import org.springframework.web.multipart.MultipartFile;

public interface ValidationUtil {
    static void assertFileContentType(MultipartFile file) {
        if (!"text/csv".equals(file.getContentType()) && !"application/vnd.ms-excel".equals(file.getContentType())) {
            throw new InvalidMimeTypeException(file.getContentType(), "Media type must be text/csv or application/vnd.ms-excel");
        }
    }
}
