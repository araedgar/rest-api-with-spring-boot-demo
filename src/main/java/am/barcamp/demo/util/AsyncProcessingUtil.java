package am.barcamp.demo.util;

import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.util.concurrent.CompletableFuture;

public interface AsyncProcessingUtil {
    static void runAsync(Runnable runnable) {
        CompletableFuture.runAsync(runnable, AutowireHelper.getBean(ThreadPoolTaskExecutor.class));
    }
}