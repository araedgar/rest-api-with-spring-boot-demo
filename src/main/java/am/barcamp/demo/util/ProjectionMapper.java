package am.barcamp.demo.util;

import org.springframework.data.projection.ProjectionFactory;

public interface ProjectionMapper {
    static <T, V> T map(Class<T> projectionType, V source) {
        return AutowireHelper.getBean(ProjectionFactory.class).createProjection(projectionType, source);
    }
}
