package am.barcamp.demo.util;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public final class AutowireHelper implements ApplicationContextAware {

    private static ApplicationContext applicationContext;

    private AutowireHelper() {
    }

    public static <T> T getBean(Class<? extends T> tClass) {
        return applicationContext.getBean(tClass);
    }

    public static <T> T getBean(Class<? extends T> tClass, String name) {
        return applicationContext.getBean(name, tClass);
    }

    @Override
    @Autowired
    public void setApplicationContext(ApplicationContext applicationContext) {
        AutowireHelper.applicationContext = applicationContext;
    }
}