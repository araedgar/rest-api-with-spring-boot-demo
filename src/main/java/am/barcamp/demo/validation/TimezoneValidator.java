package am.barcamp.demo.validation;

import am.barcamp.demo.annotation.ValidTimezone;
import am.barcamp.demo.util.LocaleProvider;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class TimezoneValidator implements ConstraintValidator<ValidTimezone, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || LocaleProvider.TIMEZONES.contains(value);
    }
}
