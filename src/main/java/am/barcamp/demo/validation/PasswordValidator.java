package am.barcamp.demo.validation;



import am.barcamp.demo.annotation.Password;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Optional.ofNullable;

public class PasswordValidator implements ConstraintValidator<Password, String> {
    private final static String REGEXP = "^(?=.*[0-9])(?=.*[a-z])(?=.*[A-Z])(?=\\S+$).{6,16}$";

    @Override
    public boolean isValid(String password, ConstraintValidatorContext constraintValidatorContext) {
        return ofNullable(password).filter(pass -> pass.matches(REGEXP)).isPresent();
    }
}
