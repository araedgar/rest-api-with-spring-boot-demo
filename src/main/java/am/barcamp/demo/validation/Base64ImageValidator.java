package am.barcamp.demo.validation;

import am.barcamp.demo.annotation.ValidBase64Image;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class Base64ImageValidator implements ConstraintValidator<ValidBase64Image, String> {
    private static final String REGEX = "^([A-Za-z0-9+/]{4})*([A-Za-z0-9+/]{3}=|[A-Za-z0-9+/]{2}==)?$";

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.matches(REGEX);
    }
}
