package am.barcamp.demo.validation;


import am.barcamp.demo.annotation.ValidRGB;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class RGBValidator implements ConstraintValidator<ValidRGB, String> {
    private final static String REGEXP = "rgb\\((?:(?:\\d{1,2}|1\\d\\d|2(?:[0-4]\\d|5[0-5])),\\s?){2}" +
            "(?:\\d{1,2}|1\\d\\d|2(?:[0-4]\\d|5[0-5]))\\)$";

    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || value.matches(REGEXP);
    }
}
