package am.barcamp.demo.validation;

import am.barcamp.demo.annotation.ValidCountry;
import am.barcamp.demo.util.LocaleProvider;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CountryValidator implements ConstraintValidator<ValidCountry, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || LocaleProvider.COUNTRY_NAMES.contains(value);
    }
}
