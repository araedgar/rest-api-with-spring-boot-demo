package am.barcamp.demo.validation;

import am.barcamp.demo.annotation.PasswordConfirm;
import am.barcamp.demo.binding.UserPayload;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import static java.util.Optional.ofNullable;

public class PasswordConfirmValidator implements ConstraintValidator<PasswordConfirm, UserPayload> {

    @Override
    public boolean isValid(UserPayload payload, ConstraintValidatorContext constraintValidatorContext) {
        return ofNullable(payload.getPassword()).filter(pass -> pass.equals(payload.getPasswordConfirm())).isPresent();
    }
}
