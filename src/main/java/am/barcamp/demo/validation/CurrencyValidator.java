package am.barcamp.demo.validation;

import am.barcamp.demo.annotation.ValidCurrency;
import am.barcamp.demo.util.LocaleProvider;
import org.springframework.stereotype.Component;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

@Component
public class CurrencyValidator implements ConstraintValidator<ValidCurrency, String> {
    @Override
    public boolean isValid(String value, ConstraintValidatorContext context) {
        return value == null || LocaleProvider.CURRENCY_CODES.contains(value);
    }
}
