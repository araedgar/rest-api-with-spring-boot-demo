package am.barcamp.demo.binding;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;
import java.util.Set;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class IdSetPayload {
    @NotEmpty
    @ApiModelProperty(required = true)
    private Set<Long> ids;
}
