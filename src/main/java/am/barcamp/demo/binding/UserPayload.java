package am.barcamp.demo.binding;

import am.barcamp.demo.annotation.Password;
import am.barcamp.demo.annotation.PasswordConfirm;
import am.barcamp.demo.annotation.ValidBase64Image;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@PasswordConfirm
public class UserPayload {
    @Email
    @NotNull
    @ApiModelProperty(required = true)
    private String username;
    @NotBlank
    @ApiModelProperty(required = true)
    private String firstName;
    @Password
    @ApiModelProperty(required = true)
    private String password;
    @NotBlank
    @ApiModelProperty(required = true)
    private String passwordConfirm;
    @NotBlank
    @ApiModelProperty(required = true)
    private String lastName;
    private String phone;
    @ValidBase64Image
    private String profilePic;
    @NotNull
    private Long officeId;
}
