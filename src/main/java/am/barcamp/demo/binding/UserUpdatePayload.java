package am.barcamp.demo.binding;

import am.barcamp.demo.annotation.ValidBase64Image;
import am.barcamp.demo.model.TwoFactorType;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserUpdatePayload {
    @Email
    @NotNull
    @ApiModelProperty(required = true)
    private String username;
    @NotBlank
    @ApiModelProperty(required = true)
    private String firstName;
    @NotBlank
    @ApiModelProperty(required = true)
    private String lastName;
    private String phone;
    @ValidBase64Image
    private String profilePic;
    private TwoFactorType twoFactorType;
}
