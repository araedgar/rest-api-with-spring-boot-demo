package am.barcamp.demo.binding;

import am.barcamp.demo.annotation.ValidCurrency;
import am.barcamp.demo.annotation.ValidRGB;
import am.barcamp.demo.model.PropertyType;
import am.barcamp.demo.model.placeholder.PolymorphicJson;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.math.BigDecimal;
import java.util.Map;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class PropertyPayload {
    @NotBlank
    private String name;
    @NotBlank
    private String sku;
    @ValidRGB
    private String color;
    @NotNull
    private PropertyType propertyType;
    private BigDecimal price;
    @ValidCurrency
    private String currency;
    @Min(1)
    private int quantity;
}
