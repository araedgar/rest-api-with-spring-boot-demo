package am.barcamp.demo.binding;

import am.barcamp.demo.annotation.ValidTimezone;
import am.barcamp.demo.model.Address;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Getter
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class OfficePayload {
    @NotBlank
    @ApiModelProperty(required = true)
    private String name;
    @NotNull
    @Valid
    @ApiModelProperty(required = true)
    private Address address;
}
