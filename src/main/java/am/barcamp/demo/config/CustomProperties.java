package am.barcamp.demo.config;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.validation.annotation.Validated;

@ConfigurationProperties(prefix = "barcamp")
@Validated
@Getter
@Setter
public class CustomProperties {
    private String signingKey;
    private boolean enableSwagger;
}
