package am.barcamp.demo.config.security;

import am.barcamp.demo.model.User;
import am.barcamp.demo.model.UserRole;
import am.barcamp.demo.repository.UserRepository;
import am.barcamp.demo.service.TwoFactorAuthenticationService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.oauth2.provider.ClientDetails;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.TokenRequest;
import org.springframework.security.oauth2.provider.request.DefaultOAuth2RequestFactory;
import org.springframework.stereotype.Component;

import java.util.Map;

@Component
public class CustomOAuth2RequestFactory extends DefaultOAuth2RequestFactory {
    private final UserRepository userRepository;
    private final TwoFactorAuthenticationService twoFactorService;

    CustomOAuth2RequestFactory(ClientDetailsService clientDetailsService, UserRepository userRepository,
                               TwoFactorAuthenticationService twoFactorService) {
        super(clientDetailsService);
        this.userRepository = userRepository;
        this.twoFactorService = twoFactorService;
    }

    @Override
    public TokenRequest createTokenRequest(Map<String, String> parameters, ClientDetails authenticatedClient) {
        final User user = userRepository.findByUsernameIgnoreCase(parameters.get("username"))
                .orElseThrow(CustomOAuth2RequestFactory::usernameNotFoundException);
        boolean isPasswordRequest = "password".equals(parameters.get("grant_type")) && parameters.get("password") != null;

        if (isPasswordRequest && user.getTwoFactorType() != null) {
            final String token = parameters.get("2fa_token");
            if (user.isAccountNonLocked()) {
                user.setLocked(true);
                twoFactorService.generateVerificationCode(user);
            } else if (token != null && twoFactorService.isValidCode(user, token)) {
                user.setLocked(false);
            }
            userRepository.save(user);
        }
        return super.createTokenRequest(parameters, authenticatedClient);
    }


    private static UsernameNotFoundException usernameNotFoundException() {
        return new UsernameNotFoundException("Not found");
    }
}