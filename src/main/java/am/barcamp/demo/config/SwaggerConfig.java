package am.barcamp.demo.config;

import am.barcamp.demo.model.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger.web.UiConfiguration;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
    @Value("${barcamp.enable-swagger:false}")
    private boolean enableSwagger;

    @Bean
    public Docket api() {
        return new Docket(DocumentationType.SWAGGER_2).select()
                .apis(RequestHandlerSelectors.basePackage("am.barcamp.demo.controller"))
                .paths(PathSelectors.any())
                .build()
                .enable(enableSwagger)
                .ignoredParameterTypes(User.class)
                .apiInfo(apiInfo())
                .securitySchemes(apiKey())
                .securityContexts(List.of(securityContext()));
    }

    private ApiInfo apiInfo() {
        return new ApiInfo("REST API", "API Documentation", "1.0",
                "urn:tos", new Contact("Edgar Arakelyan", "", "ea@barc.am"),
                null, "http://www.apache.org/licenses/LICENSE-2.0", List.of());
    }

    @Bean
    public UiConfiguration uiConfig() {
        return new UiConfiguration(null, "none", "alpha", "schema",
                UiConfiguration.Constants.DEFAULT_SUBMIT_METHODS,
                false, true, 540000L);
    }

    private List<ApiKey> apiKey() {
        return List.of(new ApiKey(HttpHeaders.AUTHORIZATION, HttpHeaders.AUTHORIZATION, "header"));
    }

    private SecurityContext securityContext() {
        return SecurityContext.builder().securityReferences(defaultAuth()).build();
    }

    private List<SecurityReference> defaultAuth() {
        return List.of(new SecurityReference(HttpHeaders.AUTHORIZATION,
                new AuthorizationScope[]{new AuthorizationScope("global", "accessEverything")}));
    }
}