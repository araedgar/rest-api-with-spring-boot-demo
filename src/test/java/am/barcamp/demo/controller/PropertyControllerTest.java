package am.barcamp.demo.controller;

import am.barcamp.demo.Application;
import am.barcamp.demo.binding.IdSetPayload;
import am.barcamp.demo.binding.PropertyPayload;
import am.barcamp.demo.model.*;
import am.barcamp.demo.repository.OfficeRepository;
import am.barcamp.demo.repository.PropertyRepository;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Currency;
import java.util.Map;
import java.util.Set;

import static org.mockito.BDDMockito.given;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
@AutoConfigureMockMvc
public class PropertyControllerTest {
    @Autowired
    private ObjectMapper mapper;
    @Autowired
    private PropertyRepository propertyRepository;
    @Autowired
    private OfficeRepository officeRepository;

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private UserDetailsService userDetailsService;

    private Property property;


    private final static String USERNAME = "michjack@gmail.com";
    private final static Long USER_ID = 1L;

    @Before
    public void setUp() {
        final Office office = officeRepository.save(getOffice());
        property = getProperty(office);
        given(userDetailsService.loadUserByUsername(USERNAME)).willReturn(getUser(office));
    }

    @After
    public void tearDown() {
        officeRepository.deleteAll();
        propertyRepository.deleteAll();
    }

    @Test
    public void create() throws Exception {
        mockMvc.perform(post("/properties")
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(getPropertyPayload())))
                .andExpect(status().isCreated())
                .andDo(print());
    }

    @Test
    public void update() throws Exception {
        propertyRepository.save(property);
        PropertyPayload payload = getPropertyPayload();
        mockMvc.perform(put("/properties/{id}", property.getId())
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(payload)))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.quantity").value(payload.getQuantity()))
                .andExpect(jsonPath("$.price").value(payload.getPrice()))
                .andExpect(jsonPath("$.id").isNotEmpty())
                .andDo(print());
    }

    @Test
    public void getAll() throws Exception {
        propertyRepository.save(property);

        mockMvc.perform(get("/properties")
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader()))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void getById() throws Exception {
        propertyRepository.save(property);

        mockMvc.perform(get("/properties/{id}", property.getId())
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader()))
                .andExpect(status().isOk())
                .andDo(print());
    }

    @Test
    public void remove() throws Exception {
        propertyRepository.save(property);

        mockMvc.perform(delete("/properties/{id}", property.getId())
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader()))
                .andExpect(status().isNoContent());
        Assert.assertEquals(0L, propertyRepository.count());
    }

    @Test
    public void removeMultiple() throws Exception {
        propertyRepository.save(property);

        mockMvc.perform(post("/properties/delete")
                .header(HttpHeaders.AUTHORIZATION, getAuthorizationHeader())
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON)
                .content(mapper.writeValueAsString(IdSetPayload.builder()
                        .ids(Set.of(property.getId(), 100L))
                        .build()
                )))
                .andExpect(status().isOk())
                .andDo(print());
        Assert.assertEquals(0L, propertyRepository.count());
    }

    private static Property getProperty(Office office) {
        return Property.builder()
                .office(office)
                .propertyType(PropertyType.FURNITURE)
                .name("Big Table")
                .sku("0065")
                .price(BigDecimal.valueOf(30000L))
                .currency(Currency.getInstance("AMD"))
                .quantity(2)
                .color("rgb(248, 137, 13)")
                .build();
    }

    private static PropertyPayload getPropertyPayload() {
        return PropertyPayload.builder()
                .propertyType(PropertyType.FURNITURE)
                .name("Big Table")
                .sku("0065")
                .price(BigDecimal.valueOf(20000L))
                .currency("AMD")
                .quantity(1)
                .color("rgb(248, 137, 13)")
                .build();
    }

    private static User getUser(Office office) {
        return User.builder()
                .id(USER_ID)
                .firstName("Michael")
                .lastName("Jackson")
                .username(USERNAME)
                .office(office)
                .role(UserRole.ROLE_OFFICE_MANAGER)
                .build();
    }

    private static Office getOffice() {
        return Office.builder()
                .address(getAddress())
                .name("office")
                .build();
    }

    private static Address getAddress() {
        return Address.builder()
                .address1("19 Baghramyan str")
                .address2("247")
                .city("Yerevan")
                .country("Armenia")
                .phone("98655522")
                .email("247@parliament.am")
                .timezone("Asia/Yerevan")
                .build();
    }

    private static String getAuthorizationHeader() {
        return "Bearer " + Jwts.builder()
                .setClaims(Map.of("userId", USER_ID, "user_name", USERNAME))
                .signWith(SignatureAlgorithm.HS256, "123".getBytes()).compact();
    }
}